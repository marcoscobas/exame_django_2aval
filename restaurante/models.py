from django.db import models

class Comida(models.Model):
    
    nome = models.CharField(max_length = 20)
    descripcion = models.CharField(max_length = 200)
    precio = models.FloatField()
    imaxe = models.ImageField(null=True, blank=True, upload_to='images/')

