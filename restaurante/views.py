from django.shortcuts import render
from .models import Comida

def home(request):
    return render(request, 'restaurante/index.html',{
        'comidas': Comida.objects.all(),
        'bodyclass': ''
    })


def about(request):
    return render(request, 'restaurante/about.html',{
        'bodyclass': 'sub_page'
    })


def menu(request):
    return render(request, 'restaurante/menu.html',{
        'comidas': Comida.objects.all(),
        'bodyclass': 'sub_page'
    })


def book(request):
    return render(request, 'restaurante/book.html',{
        'bodyclass': 'sub_page'
    })
